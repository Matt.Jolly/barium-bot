import irctokens

def test():
    global wierdassvar
    wierdassvar = 1
def check_access(func):
    def inner(*, line, _send, command, owner, **kwargs):
        if line.tags is not None and line.tags.get("account")==owner:
            func(line, _send, command, owner, **kwargs)
        else:
            channel_reply = line.params[0]
            print(line.params[0])
            to_send = irctokens.build("PRIVMSG", [channel_reply, f"{line.hostmask.nickname}: You are not authorized to perform that command."])
            _send(to_send)
            return
    return inner

@check_access
def op(line, _send, command, owner, **kwargs):
    channel_reply = line.params[0]
    arg = command.split()
    if len(arg) >= 2:
        to_send = irctokens.build("MODE", [channel_reply, "+o", arg[1]])
    else:
        to_send = irctokens.build("PRIVMSG", [channel_reply, "Command 'op' requires an argument."])
    _send(to_send)

@check_access
def deop(line, _send, command, owner, **kwargs):
    channel_reply = line.params[0]
    arg = command.split()
    if len(arg) >= 2:
        to_send = irctokens.build("MODE", [channel_reply, "-o", arg[1]])
    else:
        to_send = irctokens.build("PRIVMSG", [channel_reply, "Command 'deop' requires an argument."])
    _send(to_send)

@check_access
def voice(line, _send, command, owner, **kwargs):
    channel_reply = line.params[0]
    arg = command.split()
    if len(arg) >= 2:
        to_send = irctokens.build("MODE", [channel_reply, "+v", arg[1]])
    else:
        to_send = irctokens.build("PRIVMSG", [channel_reply, "Command 'voice' requires an argument."])
    _send(to_send)

@check_access
def devoice(line, _send, command, owner, **kwargs):
    channel_reply = line.params[0]
    arg = command.split()
    if len(arg) >= 2:
        to_send = irctokens.build("MODE", [channel_reply, "-v", arg[1]])
    else:
        to_send = irctokens.build("PRIVMSG", [channel_reply, "Command 'devoice' requires an argument."])
    _send(to_send)

@check_access
def kick(line, _send, command, owner, **kwargs):
    channel_reply = line.params[0]
    arg = command.split()
    if len(arg) >= 2:
        to_send = irctokens.build("KICK", [channel_reply, arg[1], arg[1]])
    else:
        to_send = irctokens.build("PRIVMSG", [channel_reply, "Command 'kick' requires an argument."])
    _send(to_send)


def version(*, line, _send, **kwargs):
    channel_reply = line.params[0]
    print(line.params[0])
    to_send = irctokens.build("PRIVMSG", [channel_reply, f"{line.hostmask.nickname}: Barium: version 1.0, Gentoo GNU/Linux"])
    _send(to_send)
def code(*, line, _send, **kwargs):
    channel_reply = line.params[0]
    print(line.params[0])
    to_send = irctokens.build("PRIVMSG", [channel_reply, f"{line.hostmask.nickname}: Barium: https://gitlab.com/rahul.sandhu/barium-bot"])
    _send(to_send)

@check_access
def quit_irc(line, _send, command, owner, **kwargs):
    channel_reply = line.params[0]
    arg = command.split()
    if len(arg) >= 2:
        to_send = irctokens.build("PRIVMSG", [channel_reply, arg[1:]])
    else:
        to_send = irctokens.build("PRIVMSG", [channel_reply, "Exiting"])
    _send(to_send)
    exit(99)

