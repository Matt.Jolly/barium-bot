import irctokens
import requests
import sys
import traceback
import urllib

def tinyurl(line, _send, command):
    channel_reply = line.params[0]
    url_capture = line.params[1]
    url_long = url_capture[9:]
    URL = "http://tinyurl.com/api-create.php"
    try:
        url = URL + "?" \
            + urllib.parse.urlencode({"url": url_long})
        res = requests.get(url)
    except Exception as e:
        raise
    try:
        shorten(url_long)
    except Exception as e:
        traceback.print_exc()
    to_send = irctokens.build("PRIVMSG", [channel_reply, res.text])
    _send(to_send)
