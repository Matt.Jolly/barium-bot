import irctokens, random
def ping(*, line, _send, **kwargs):
    channel_reply = line.params[0]
    to_send = irctokens.build("PRIVMSG", [channel_reply, "pong"])
    _send(to_send)
def ajak(*, line, _send, **kwargs):
    channel_reply = line.params[0]
    to_send = irctokens.build("PRIVMSG", [channel_reply, "sure"])
    _send(to_send)
def sandcat(line, _send):
    channel_reply = line.params[0]
    to_send = irctokens.build("PRIVMSG", [channel_reply, "Jess is a sandcat"])
    _send(to_send)
    if line.hostmask.nickname == "jess":
        to_send2 = irctokens.build("PRIVMSG", [channel_reply, "Oh hey jess, meow"])
        _send(to_send2)
    elif line.hostmask.nickname == "oldfashionedcow":
        to_send2 = irctokens.build("PRIVMSG", [channel_reply, "Oh hey oldfashionedcow, moo"])
        _send(to_send2)
def shank(*, line, _send, owner, **kwargs):
    channel_reply = line.params[0]
    if line.tags is not None and line.tags.get("account")=="username234":
        to_send = irctokens.build("PRIVMSG", [channel_reply, "\x01ACTION Shanks username234 with the SUPER rusty jagged long one mutiple times\x01"])
        _send(to_send)
    if line.tags is not None and line.tags.get("account")==owner:
        to_send = irctokens.build("PRIVMSG", [channel_reply, f"{owner}: take this shank"])
        _send(to_send)
    elif line.tags is not None and line.tags.get("account")=="tdr":
        to_send = irctokens.build("PRIVMSG", [channel_reply, "God sakes shank master. OLDFASHIONEDCOW is a COW."])
        _send(to_send)
        to_send = irctokens.build("PRIVMSG", [channel_reply, "\x01ACTION hands tdr with the SUPER rusty jagged long one mutiple times\x01"])
        _send(to_send)
    else:
        _send(irctokens.build("PRIVMSG", [channel_reply, "\x01ACTION shanks * with the SUPER rusty jagged long one mutiple times\x01"]))
def bahhumbug(*, line, _send, **kwargs):
    channel_reply = line.params[0]
    to_send = irctokens.build("PRIVMSG", [channel_reply, "It was a typo. Stuff happens."])
    _send(to_send)
def censored(*, line, _send, **kwargs):
    channel_reply = line.params[0]
    to_send = irctokens.build("PRIVMSG", [channel_reply, "ASM is technically type safe."])
    _send(to_send)
def time(*, line, _send, command, **kwargs):
    channel_reply = line.params[0]
    if command == ".time tdr":
        to_send = irctokens.build("PRIVMSG", [channel_reply, "NO FUCKING SHANKING AT ALL! LIES!"])
        _send(to_send)
    else:
        _send(irctokens.build("PRIVMSG", [channel_reply, "GO READ A GODDAMN CLOCK!"]))
def tdr(*, line, _send, **kwargs):
    channel_reply = line.params[0]
    to_send = irctokens.build("PRIVMSG", [channel_reply, "No fucking swearing at all! LIES!"])
    _send(to_send)
def eightball(*, line, _send, **kwargs):
    questions = ["It is certain","Outlook good","You may rely on it","Ask again later","Concentrate and ask again","Reply hazy, try again","My reply is no","My sources say no"]
    answer = random.randint(1,8)
    channel_reply = line.params[0]
    to_send = irctokens.build("PRIVMSG", [channel_reply, questions[answer-1]])
    _send(to_send)
def botsnack(*, line, _send, **kwargs):
    channel_reply = line.params[0]
    to_send = irctokens.build("PRIVMSG", [channel_reply, "Yum! Thank you!"])
    _send(to_send)
def metanova(*, line, _send, **kwargs):
    channel_reply = line.params[0]
    to_send = irctokens.build("PRIVMSG", [channel_reply, "/msg chanserv op $CHANNEL"])
    _send(to_send)
def roulette(*, line, _send, **kwargs):
    channel_reply = line.params[0]
    user_reply = line.hostmask.nickname
    should_kick = random.randint(1,3)
    if should_kick == 1:
        print("should_kick is 1")
        to_send = irctokens.build("KICK", [channel_reply, user_reply, "Bang!"])
        _send(to_send)
    elif should_kick == 2:
        print("should_kick is 2")
        to_send = irctokens.build("PRIVMSG", [channel_reply, f"{line.hostmask.nickname}: *click*"])
        _send(to_send)
    else:
        print("should_kick is 3")
        to_send = irctokens.build("PRIVMSG", [channel_reply, "*BANG* Hey, who put a blank in here?!"])
        to_send2 = irctokens.build("PRIVMSG", [channel_reply, "\x01ACTION spins and reloads the chambers.\x01"])
        _send(to_send)
        _send(to_send2)
def tea(*, line, _send, command, **kwargs):
    channel_reply = line.params[0]
    command.split()
    if len(command) < 1:
        to_send = irctokens.build("PRIVMSG", [channel_reply, f"Have some fresh tea, {line.hostmask.nickname}!"])
    else:
        to_send = irctokens.build("PRIVMSG", [channel_reply, f"Have some fresh tea, {command[5:]}!"])
    _send(to_send)
def coffee(*, line, _send, **kwargs):
    channel_reply = line.params[0]
    to_send = irctokens.build("PRIVMSG", [channel_reply, f"Have some fresh coffee, {line.hostmask.nickname}!"])
    _send(to_send)
def soda(*, line, _send, **kwargs):
    channel_reply = line.params[0]
    to_send = irctokens.build("PRIVMSG", [channel_reply, f"Have some fresh soda, {line.hostmask.nickname}!"])
    _send(to_send)
def sweettea(*, line, _send, **kwargs):
    channel_reply = line.params[0]
    to_send = irctokens.build("PRIVMSG", [channel_reply, f"Have some fresh US Southern sweet tea, {line.hostmask.nickname}!"])
    _send(to_send)
def icedtea(*, line, _send, **kwargs):
    channel_reply = line.params[0]
    to_send = irctokens.build("PRIVMSG", [channel_reply, f"Have some fresh US Southern unsweetened iced tea, {line.hostmask.nickname}!"])
    _send(to_send)
def gentoo(*, line, _send, **kwargs):
    channel_reply = line.params[0]
    _send(irctokens.build("PRIVMSG", [channel_reply,"         -/oyddmdhs+:."]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"     -odNMMMMMMMMNNmhy+-`"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"   -yNMMMMMMMMMMMNNNmmdhy+-"]))
    _send(irctokens.build("PRIVMSG", [channel_reply," `omMMMMMMMMMMMMNmdmmmmddhhy/`"]))
    _send(irctokens.build("PRIVMSG", [channel_reply," omMMMMMMMMMMMNhhyyyohmdddhhhdo`"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,".ydMMMMMMMMMMdhs++so/smdddhhhhdm+`"]))
    _send(irctokens.build("PRIVMSG", [channel_reply," oyhdmNMMMMMMMNdyooydmddddhhhhyhNd."]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"  :oyhhdNNMMMMMMMNNNmmdddhhhhhyymMh"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"    .:+sydNMMMMMNNNmmmdddhhhhhhmMmy"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"       /mMMMMMMNNNmmmdddhhhhhmMNhs:"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"    `oNMMMMMMMNNNmmmddddhhdmMNhs+`"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"  `sNMMMMMMMMNNNmmmdddddmNMmhs/."]))
    _send(irctokens.build("PRIVMSG", [channel_reply," /NMMMMMMMMNNNNmmmdddmNMNdso:`"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"+MMMMMMMNNNNNmmmmdmNMNdso/-"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"yMMNNNNNNNmmmmmNNMmhs+/-`"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"/hMMNNNNNNNNMNdhs++/-`"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"`/ohdmmddhys+++/:.`"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"  `-//////:--."]))
def fedora(*, line, _send, **kwargs):
    channel_reply = line.params[0]
    _send(irctokens.build("PRIVMSG", [channel_reply,"          /:-------------:\\"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"       :-------------------::"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"     :-----------/shhOHbmp---:\\"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"  /-----------omMMMNNNMMD  ---:"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"  :-----------sMMMMNMNMP.    ---:"]))
    _send(irctokens.build("PRIVMSG", [channel_reply," :-----------:MMMdP-------    ---\\"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,",------------:MMMd--------    ---:"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,":------------:MMMd-------    .---:"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,":----    oNMMMMMMMMMNho     .----:"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,":--     .+shhhMMMmhhy++   .------/"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,":-    -------:MMMd--------------:"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,":-   --------/MMMd-------------;"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,":-    ------/hMMMy------------:"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,":-- :dMNdhhdNMMNo------------;"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,":---:sdNMMMMNds:------------:"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,":------:://:-------------::"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,":---------------------://"]))
def tux(*, line, _send, **kwargs):
    channel_reply = line.params[0]
    _send(irctokens.build("PRIVMSG", [channel_reply,"        #####"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"       #######"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"       ##O#O##"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"       #######"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"     ###########"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"    #############"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"   ###############"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"   ################"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"  #################"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"#####################"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"#####################"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"  #################"]))
