import irctokens
import yaml
import pytz
from datetime import datetime
from pathlib import Path
from pytz import timezone
def date(*, line, _send, command, **kwargs):
    print(type(pytz.all_timezones))
    tokens = command.split(' ')
    format = "%Y-%m-%d %H:%M:%S %Z%z"
    try:
        with open("data/timezones.yml", "r") as stream:
            user_times = yaml.safe_load(stream)
    except:
        print("Timezone data file (timezones.yml) not found.")
    command.split()
    if command[6:11] == "--set" and len(tokens) >=3:
        nick=line.hostmask.nickname
        if line.hostmask.nickname in user_times:
            channel_reply = line.params[0]
            if command.split(' ')[2] in pytz.all_timezones:
                with open('data/timezones.yml', 'r') as user_times_ro:
                    lines = user_times_ro.readlines()
                    with open('data/timezones.yml', 'w') as user_times_rw:
                        for line in lines:
                            if not line.startswith(f"{nick}:"):
                                user_times_rw.write(line)
                user_times_rw.close()
                tz_info = f"{nick}: \"{command.split(' ')[2]}\""
                user_times_rw = open("data/timezones.yml", "a")
                user_times_rw.write(f"{tz_info}\n")
                user_times_rw.close()
                _send(irctokens.build("PRIVMSG", [channel_reply, f"{nick}: ack!"]))
            else:
                _send(irctokens.build("PRIVMSG", [channel_reply, f"{nick}: I don't know where '{tokens[2]}' is. Please specify a known time in the format 'Your/Timezone'."]))
        else:
            channel_reply = line.params[0]
            if command.split(' ')[2] in pytz.all_timezones:
                channel_reply = line.params[0]
                tz_info = f"{line.hostmask.nickname}: \"{command.split(' ')[2]}\""
                user_times_rw = open("data/timezones.yml", "a")
                user_times_rw.write(f"{tz_info}\n")
                user_times_rw.close()
                _send(irctokens.build("PRIVMSG", [channel_reply, f"{nick}: ack!"]))
            else:
                _send(irctokens.build("PRIVMSG", [channel_reply, f"{nick}: I don't know where '{tokens[2]}' is. Please specify a known time in the format 'Your/Timezone'."]))
    elif not command[5:]:
        channel_reply = line.params[0]
        if line.hostmask.nickname in user_times:
            now_utc = datetime.now(timezone('UTC'))
            timezone_user = now_utc.astimezone(timezone(user_times[line.hostmask.nickname]))
            _send(irctokens.build("PRIVMSG", [channel_reply, f"{line.hostmask.nickname}: {timezone_user.strftime(format)}"]))
        else:
            _send(irctokens.build("PRIVMSG", [channel_reply, f"{line.hostmask.nickname}: I don't know where you are. Please use <commandkey>date --set Your/Timezone"]))
    elif len(tokens) >= 2:
        channel_reply = line.params[0]
        if tokens[1] in user_times:
            now_utc = datetime.now(timezone('UTC'))
            timezone_user = now_utc.astimezone(timezone(user_times[tokens[1]]))
            _send(irctokens.build("PRIVMSG", [channel_reply, f"{line.hostmask.nickname}: {timezone_user.strftime(format)}"]))
        else:
            _send(irctokens.build("PRIVMSG", [channel_reply, f"{line.hostmask.nickname}: I don't know where '{tokens[1]}' is. Please tell them to use <commandkey>date --set Your/Timezone"]))

