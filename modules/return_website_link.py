import irctokens
import re
import requests
from bs4 import BeautifulSoup

def trim_url(url):
    while url[-1] in '.,?!\'":;':
        url = url[:-1]

    for (opener, closer) in [('(', ')'), ('[', ']'), ('{', '}'), ('<', '>')]:
        if url[-1] == closer and url.count(opener) < url.count(closer):
            url = url[:-1]
    return url

def extract_urls(command):
    exclusion_char=None
    url = command
    schemes = ['http', 'https']
    schemes_patterns = '|'.join(re.escape(scheme) for scheme in schemes)
    re_url = r'((?:%s)(?::\/\/\S+))' % schemes_patterns
    if exclusion_char is not None:
        re_url = r'((?<!%s)(?:%s)(?::\/\/\S+))' % (
            exclusion_char, schemes_patterns)

    r = re.compile(re_url, re.IGNORECASE | re.UNICODE)
    urls = re.findall(r, url)
    urls = (trim_url(url) for url in urls)
    global extracted_urls
    extracted_urls=[]
    seen = set()
    for url in urls:
        try:
            url = iri_to_uri(url)
        except Exception:
            extracted_urls.append(url)

def return_website_link(line, _send, command):
    extract_urls(command)
    channel_reply = line.params[0]
    for i in range(len(extracted_urls)):
        try:
            reqs = requests.get(extracted_urls[i])
            soup = BeautifulSoup(reqs.text, 'html.parser')
            for title in soup.find_all('title'):
                title.get_text()
            print(title.get_text())
            to_send = irctokens.build("PRIVMSG", [channel_reply, title.get_text()])
            _send(to_send)
        except requests.exceptions.ConnectionError:
            print("not found")
        except:
            print("Error in handling url")
