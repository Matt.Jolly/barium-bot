import socket
import ssl
import irctokens
import base64
import yaml
from modules import *

# Opening the configuration file for reading
with open("config.yml", "r") as stream:
    configuration = yaml.safe_load(stream)

# Parsing variables from the configuration file
NICK = configuration["nick"]
CHAN = configuration["channel"]
NICKEMAIL = configuration["nickserv_email"]
NICKPASSWD = configuration ["nickserv_password"]
commandkey = configuration["commandkey"]
owner = configuration["owner_account"]
server = configuration["server"]
port = configuration["port"]
usessl = configuration["use_ssl"]
gentoo_bugzilla_api_key = configuration["gentoo_bugzilla_api_key"]
d = irctokens.StatefulDecoder()
e = irctokens.StatefulEncoder()

# Creating socket
if usessl==True:
    context = ssl.create_default_context()
    socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s = context.wrap_socket(socket, server_hostname=server)
    s.connect((server, port))
else:
    s = socket.socket()
    s.connect((server, port))


def _send(line):
    e.push(line)
    while e.pending():
        e.pop(s.send(e.pending()))
_send(irctokens.build("CAP", ["REQ", "sasl account-tag"]))
_send(irctokens.build("AUTHENTICATE", ["PLAIN"]))
_send(irctokens.build("AUTHENTICATE", [base64.b64encode(f"{NICK}\x00{NICK}\x00{NICKPASSWD}".encode()).decode()]))
_send(irctokens.build("USER", ["username", "0", "*", "barium 0.1"]))
_send(irctokens.build("NICK", [NICK]))

# Command dictionary (stores all possible commands, with the functions to run on their execution)
func_commands_dict={
    "ping": ping,
    "sandcat": sandcat,
    "bahhumbug": bahhumbug,
    "shank": shank,
    "eightball": eightball,
    "8ball": eightball,
    "time": date,
    "date": date,
    "botsnack": botsnack,
    "tinyurl": tinyurl,
    "tdr": tdr,
    "roulette": roulette,
    "gentoo": gentoo,
    "fedora": fedora,
    "tux": tux,
    "op": op,
    "deop": deop,
    "voice": voice,
    "devoice": devoice,
    "version": version,
    "code": code,
    "source": code,
    "src": code,
    "ajak": ajak,
    "tea": tea,
    "coffee": coffee,
    "soda": soda,
    "sweettea": sweettea,
    "icedtea": icedtea,
    "xxc3nsoredxx": censored,
    "metanova": metanova,
    "censored": censored,
    "bug": bug,
    "quit": quit_irc
}
line = None

while True:
    lines = d.push(s.recv(1024))
    if lines is None:
        print("! disconnected")
        break
    for line in lines:
        print(f"< {line.format()}")
        if line.command in {"903", "904"}:
            _send(irctokens.build("CAP", ["END"]))
        if line.command == "PING":
            to_send = irctokens.build("PONG", [line.params[0]])
            _send(to_send)
        elif line.command == "001":
            to_send = irctokens.build("JOIN", [CHAN])
            _send(to_send)
        elif line.command == "PRIVMSG":
            command=line.params[-1]
            if line.params[-1].startswith(commandkey):
                try:
                    func_commands_dict[command.split()[0][1:]](**{
                        "line": line,
                        "_send": _send,
                        "command": command,
                        "owner": owner,
                        "gentoo_bugzilla_api_key": gentoo_bugzilla_api_key
                    })
                except KeyError:
                    print(f"{command} not found")
            if "https://" in command or "http://" in command:
                return_website_link(line, _send, command)
